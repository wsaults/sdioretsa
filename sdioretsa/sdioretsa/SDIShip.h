//
//  SDIShip.h
//  sdioretsa
//
//  Created by Will Saults on 11/25/13.
//  Copyright (c) 2013 AppVentures LLC. All rights reserved.
//

#import "SDIEnemyCharacter.h"

#define kLifeShipSize (CGSizeMake(10, 10))

@interface SDIShip : SDIEnemyCharacter

- (id)initAtPosition:(CGPoint)position;
- (void)fireProjectile;
- (SKSpriteNode *)projectile;
- (SKEmitterNode *)projectileEmitter;
- (SKAction *)projectileSoundAction;

@end

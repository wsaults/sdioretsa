//
//  SDIEvadeAndAttackAI.h
//  sdioretsa
//
//  Created by Will Saults on 12/2/13.
//  Copyright (c) 2013 AppVentures LLC. All rights reserved.
//

#import "SDIArtificialIntelligence.h"

#define kEnemyAlertRadius (kCharacterCollisionRadius * 500)

@interface SDIEvadeAndAttackAI : SDIArtificialIntelligence

@property (nonatomic) CGFloat dodgeRadius;
@property (nonatomic) CGFloat maxAlertRadius;

@end

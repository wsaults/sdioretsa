//
//  SDIGameplayScene.h
//  sdioretsa
//
//  Created by Will Saults on 11/25/13.
//  Copyright (c) 2013 AppVentures LLC. All rights reserved.
//

#import "SDIMultiLayerScene.h"

@interface SDIGameplayScene : SDIMultiLayerScene <SKPhysicsContactDelegate>

- (void)startGame;
- (void)endGameWithWin:(BOOL)win;


@end

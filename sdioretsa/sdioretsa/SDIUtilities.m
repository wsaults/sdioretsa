//
//  SDIUtilities.m
//  sdioretsa
//
//  Created by Will Saults on 11/27/13.
//  Copyright (c) 2013 AppVentures LLC. All rights reserved.
//

#pragma mark - Point Calculations
CGFloat SDIDistanceBetweenPoints(CGPoint first, CGPoint second)
{
    return hypotf(second.x - first.x, second.y - first.y);
}

CGFloat SDIRadiansBetweenPoints(CGPoint first, CGPoint second)
{
    CGFloat deltaX = second.x - first.x;
    CGFloat deltaY = second.y - first.y;
    return atan2f(deltaY, deltaX);
}

CGPoint SDIPointByAddingCGPoints(CGPoint first, CGPoint second)
{
    return CGPointMake(first.x + second.x, first.y + second.y);
}

#pragma mark - Loading from a Texture Atlas
NSArray *SDILoadFramesFromAtlas(NSString *atlasName, NSString *baseFileName, int numberOfFrames) {
    NSMutableArray *frames = [NSMutableArray arrayWithCapacity:numberOfFrames];
    
    SKTextureAtlas *atlas = [SKTextureAtlas atlasNamed:atlasName];
    for (int i = 1; i <= numberOfFrames; i++) {
        NSString *fileName = [NSString stringWithFormat:@"%@%d.png", baseFileName, i];
        SKTexture *texture = [atlas textureNamed:fileName];
        [frames addObject:texture];
    }
    
    return frames;
}

#pragma mark - Emitters
void SDIRunOneShotEmitter(SKEmitterNode *emitter, CGFloat duration)
{
    [emitter runAction:[SKAction sequence:@[
                                            [SKAction waitForDuration:duration],
                                            [SKAction runBlock:^{
                                                emitter.particleBirthRate = 0;
                                            }],
                                            [SKAction waitForDuration:emitter.particleLifetime + emitter.particleLifetimeRange],
                                            [SKAction removeFromParent],
                                            ]]];
}

void SDIRunNumOfParticlesEmitter(SKEmitterNode *emitter, NSUInteger numOfParticles)
{
    [emitter runAction:[SKAction sequence:@[[SKAction runBlock:^{
                                            emitter.numParticlesToEmit = numOfParticles;
    }],
                                            [SKAction waitForDuration:emitter.particleLifetime + emitter.particleLifetimeRange],
                                            [SKAction removeFromParent],
                                            ]]];
}


#pragma mark - NSValue Category
@implementation NSValue (SDIAdditions)
- (CGPoint)sdi_CGPointValue {
    return [self CGPointValue];
}

+ (instancetype)sdi_valueWithCGPoint:(CGPoint)point
{
    return [self valueWithCGPoint:point];
}
@end



#pragma mark - SKEmitterNode Category
@implementation SKEmitterNode (SDIAdditions)
+ (instancetype)sdi_emitterNodeWithEmitterNamed:(NSString *)emitterFileName
{
    return [NSKeyedUnarchiver unarchiveObjectWithFile:[[NSBundle mainBundle] pathForResource:emitterFileName ofType:@"sks"]];
}
@end
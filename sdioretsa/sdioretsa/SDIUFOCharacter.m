//
//  SDIUFOCharacter.m
//  sdioretsa
//
//  Created by Will Saults on 12/12/13.
//  Copyright (c) 2013 AppVentures LLC. All rights reserved.
//

#import "SDIUFOCharacter.h"
#import "SDIMultiLayerScene.h"
#import "SDIUtilities.h"

#define kSpriteName @"UFO"
#define kDefaultNumberOfMovingFrames 10

@implementation SDIUFOCharacter

- (id)initAtPosition:(CGPoint)position
{
    SKTextureAtlas *atlas = [SKTextureAtlas atlasNamed:@"UFO"];
    self = [super initWithTexture:[atlas textureNamed:@"ufo1.png"] atPosition:position];
    if (self) {
        self.name = @"UFO";
        self.requestedAnimation = SDIAnimationStateMove;
        NSLog(@"created ufo %@", self);
    }
    return self;
}

- (void)updateWithTimeSinceLastUpdate:(CFTimeInterval)interval
{
    [super updateWithTimeSinceLastUpdate:interval];
}

#pragma mark - Shared Assets
+ (void)loadSharedAssets
{
    [super loadSharedAssets];
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sSharedMoveAnimationFrames = SDILoadFramesFromAtlas(@"UFO", @"ufo", kDefaultNumberOfMovingFrames);
    });
}

static NSArray *sSharedMoveAnimationFrames = nil;
- (NSArray *)moveAnimationFrames {
    return sSharedMoveAnimationFrames;
}

@end

//
//  SDIArtificialIntelligence.m
//  sdioretsa
//
//  Created by Will Saults on 12/2/13.
//  Copyright (c) 2013 AppVentures LLC. All rights reserved.
//

#import "SDIArtificialIntelligence.h"

@implementation SDIArtificialIntelligence

#pragma mark - Initialization
- (id)initWithCharacter:(SDICharacter *)character target:(SDICharacter *)target {
    self = [super init];
    if (self) {
        _character = character;
        _target = target;
    }
    return self;
}

#pragma mark - Loop Update
- (void)updateWithTimeSinceLastUpdate:(CFTimeInterval)interval {
    /* Overridden by subclasses. */
}

#pragma mark - Targets
- (void)clearTarget:(SDICharacter *)target {
    if (self.target == target) {
        self.target = nil;
    }
}

@end

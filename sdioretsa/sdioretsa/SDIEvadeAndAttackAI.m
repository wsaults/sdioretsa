//
//  SDIEvadeAndAttackAI.m
//  sdioretsa
//
//  Created by Will Saults on 12/2/13.
//  Copyright (c) 2013 AppVentures LLC. All rights reserved.
//

#import "SDIEvadeAndAttackAI.h"
#import "SDICharacter.h"
#import "SDIShip.h"
#import "SDIAsteroid.h"
#import "SDIMultiLayerScene.h"
#import "SDIUtilities.h"

@implementation SDIEvadeAndAttackAI

#pragma mark - Initialization
- (id)initWithCharacter:(SDICharacter *)character target:(SDICharacter *)target {
    self = [super initWithCharacter:character target:target];
    if (self) {
    }
    return self;
}

#pragma mark - Loop Update
- (void)updateWithTimeSinceLastUpdate:(CFTimeInterval)interval {
    SDICharacter *thisCharacter = self.character;
    
    if (thisCharacter.dying) {
        self.target = nil;
        return;
    }
    
    CGPoint position = thisCharacter.position;
    SDIMultiLayerScene *scene = [thisCharacter characterScene];
    CGFloat closestAsteroidDistance = MAXFLOAT;
    
    // Find the closest living asteroid, if any, within our alert distance.
    for (SDIAsteroid *asteroid in scene.asteroids) {
        CGPoint asteroidPosition = asteroid.position;
        CGFloat distance = SDIDistanceBetweenPoints(position, asteroidPosition);
        if (distance < kEnemyAlertRadius && distance < closestAsteroidDistance && !asteroid.dying) {
            closestAsteroidDistance = distance;
            self.target = asteroid;
        }
    }
    
    // If there's no target, don't do anything.
    SDICharacter *target = self.target;
    if (!target) {
        return;
    }
    
    // Otherwise dodge/attack the target, if it's near enough.
    CGPoint asteroidPosition = target.position;
    CGFloat dodgeRadius = self.dodgeRadius;
    
    if (closestAsteroidDistance > self.maxAlertRadius) {
        self.target = nil;
    } else if (closestAsteroidDistance > dodgeRadius) {
        // When the character is far enough from danger move back to the middle of the screen.
        CGRect frame = thisCharacter.characterScene.frame;
        CGPoint centerPoint = CGPointMake(CGRectGetMidX(frame), CGRectGetMidY(frame));
        [thisCharacter faceTo:centerPoint withDuration:0.2f];
        [thisCharacter moveTowards:centerPoint withTimeInterval:interval];
    } else if (closestAsteroidDistance < dodgeRadius) {
        // If danger is near then try to evade and destroy.
        [thisCharacter faceTo:asteroidPosition withDuration:0.2f];
        [thisCharacter evade:asteroidPosition withTimeInterval:interval];
        
        [self.character fireProjectile];
    }
}

@end

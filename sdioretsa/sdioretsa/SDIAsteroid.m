//
//  SDIAsteroid.m
//  sdioretsa
//
//  Created by Will Saults on 11/25/13.
//  Copyright (c) 2013 AppVentures LLC. All rights reserved.
//

#import "SDIAsteroid.h"
#import "SDIMultiLayerScene.h"
#import "SDIUtilities.h"

#define kSpriteName @"Asteroid"
#define kPointValue 100
#define kNumOfAsteroidsOnDeath 2

@implementation SDIAsteroid

- (id)initAtPosition:(CGPoint)position withSize:(CGSize)size withMoveDirection:(CGPoint)moveDirection
{
    self = [super initWithName:kSpriteName AtPosition:position withSize:size];
    if (self) {
        if (!CGPointEqualToPoint(moveDirection, CGPointZero)) {
            self.moveDirection = moveDirection;
        }
        // Adjust the movement speed of the asteroid based on it's size.
        self.movementSpeed = kMovementSpeed - (size.height + boris_random(1, 20));
        self.pointValue = kPointValue;
    }
    
    return self;
}

#pragma mark - Overridden Methods
- (void)configurePhysicsBody
{
    self.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:self.size.width/2];
    
    // Our object type for collisions.
    self.physicsBody.categoryBitMask = SDIColliderTypeAsteroid;
    
    // Collides with these objects.
    self.physicsBody.collisionBitMask = SDIColliderTypeShip | SDIColliderTypeProjectile;
    
    // We want notifications for colliding with these objects.
    self.physicsBody.contactTestBitMask = SDIColliderTypeShip | SDIColliderTypeProjectile;
}

/* Overridden Methods. */
- (void)animationDidComplete:(SDIAnimationState)animationState
{
    switch (animationState) {
        case SDIAnimationStateDeath:{
            // TODO: Prep the death emittier.
//            SKEmitterNode *emitter = [[self deathEmitter] copy];
//            emitter.zPosition = self.zPosition;
//            emitter.position = self.position;
//            [emitter setNumParticlesToEmit:10];
//            [self addChild:emitter];
//            SDIRunOneShotEmitter(emitter, 1f);
            
            SKAction *sound = [SKAction playSoundFileNamed:@"AsteroidDeath.wav" waitForCompletion:NO];
            SDIMultiLayerScene *characterScene = [self characterScene];
            
            [self runAction:[SKAction sequence:@[sound,
                                                 [SKAction waitForDuration:0.01],
                                                 [SKAction fadeOutWithDuration:0.05f],
                                                 [SKAction runBlock:^{
                
                // If asteroid size is larger than the small size create 4 smaller asteroids
                if (CGSizeEqualToSize(self.size, kLargeAsteroidSize)) {
                    [self breakIntoAsteroidsSize:kMediumAsteroidSize];
                } else if (CGSizeEqualToSize(self.size, kMediumAsteroidSize)) {
                    [self breakIntoAsteroidsSize:kSmallAsteroidSize];
                }
                
                [[characterScene asteroids] removeObject:self];
                
                // If there are no asteroids left and 0 asteroid lives left then the game is lost.
                if ([characterScene asteroids].count == 0 && [characterScene asteroidLives].count == 0) {
                    [characterScene endGameWithWin:NO];
                }
                
                [self removeFromParent];
                [self removeAllActions];
            }]]
                             ]];
            break;}
            
        default:
            break;
    }
}

- (void)breakIntoAsteroidsSize:(CGSize)size
{
    CGPoint posistion = self.position;    
    for (int i = 0; i < kNumOfAsteroidsOnDeath; i++) {
        // Create a new random direction.
        float newX = roundf(boris_random(self.moveDirection.x, abs(self.moveDirection.x) + 50));
        float newY = roundf(boris_random(self.moveDirection.y, abs(self.moveDirection.y) + 50));
        CGPoint newMoveDirection = CGPointMake(newX, newY);
        
        [[self characterScene] addAsteroidAtPosition:posistion
                                            withSize:size
                                   withMoveDirection:newMoveDirection];
    }
}

- (void)collidedWith:(SKPhysicsBody *)other
{
    if (self.isDying) {
        return;
    }
    
    if (other.categoryBitMask & SDIColliderTypeProjectile || SDIColliderTypeShip) {
        // Destroy the asteroid
        [self performDeath];
    }
}

#pragma mark - Shared Assets
+ (void)loadSharedAssets
{
    [super loadSharedAssets];
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sSharedDeathEmitter = [SKEmitterNode sdi_emitterNodeWithEmitterNamed:@"AsteroidDeath"];
    });
}

static SKEmitterNode *sSharedDeathEmitter = nil;
- (SKEmitterNode *)deathEmitter {
    return sSharedDeathEmitter;
}

@end

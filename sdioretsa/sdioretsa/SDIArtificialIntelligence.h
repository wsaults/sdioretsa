//
//  SDIArtificialIntelligence.h
//  sdioretsa
//
//  Created by Will Saults on 12/2/13.
//  Copyright (c) 2013 AppVentures LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SDICharacter;

@interface SDIArtificialIntelligence : NSObject

@property (nonatomic, weak) SDICharacter *character;
@property (nonatomic, weak) SDICharacter *target;

- (id)initWithCharacter:(SDICharacter *)character target:(SDICharacter *)target;

- (void)clearTarget:(SDICharacter *)target;

- (void)updateWithTimeSinceLastUpdate:(CFTimeInterval)interval;

@end

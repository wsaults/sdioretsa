//
//  SDIGameplayScene.m
//  sdioretsa
//
//  Created by Will Saults on 11/25/13.
//  Copyright (c) 2013 AppVentures LLC. All rights reserved.
//

#import "SDIGameplayScene.h"
#import "SDIShip.h"
#import "SDIAsteroid.h"
#import "SDIUFOCharacter.h"
#import "SDIUtilities.h"
#import "SDIConstants.h"
#import "SDILeaderBoardManager.h"

@interface SDIGameplayScene ()

@property (nonatomic, strong) NSMutableArray *particleSystems;    // Particle emitter nodes
@property (nonatomic) CGPoint gestureStartPoint;                  // The point where the user starts touching
@property (nonatomic, strong) SKLabelNode *instuctionLabel;       // A tmp label node used to give instructions
@property (nonatomic, strong) SKLabelNode *pausedLabel;           // A label node to inform the user that the game is paused.
@property (nonatomic, strong) SKLabelNode *endGameLabel;          // A label node to inform the user about the end game state.
@property (nonatomic, strong) SKLabelNode *mainMenuLabel;        // A label to send you back to the main menu.
@property (nonatomic, strong) SKLabelNode *scoredPointsLabel;     // A label node to inform the user about the number of points that they scored.
@property (nonatomic, strong) SKLabelNode *playerTotalScoreLabel; // A label node to inform the user about their total scored points.
@property (nonatomic, strong) SKSpriteNode *playAgainButton;      // A button node that replays the game.
@property (nonatomic, getter = isStarted) BOOL started;           // A boolean to check if the game has been started.
@property (nonatomic, getter = isGamePaused) BOOL gamePaused;     // A boolean to check if the game has been paused.
@property (nonatomic, getter = isGameOver) BOOL gameOver;         // A boolean to check if the game is over.

@end

@implementation SDIGameplayScene

-(id)initWithSize:(CGSize)size
{
    if (self = [super initWithSize:size]) {
        self.started = NO;
        // Init the arrays
        _particleSystems = [[NSMutableArray alloc] init];
        self.asteroids = [[NSMutableArray alloc] init];
        self.ships = [[NSMutableArray alloc] init];
        self.ufos = [[NSMutableArray alloc] init];
        self.shipLives = [[NSMutableArray alloc] init];
        self.asteroidLives = [[NSMutableArray alloc] init];
        
        [self runIntroWithSize:size];
        
        // Add the BG image.
        SKSpriteNode *bg = nil;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            bg = [SKSpriteNode spriteNodeWithImageNamed:@"BG-iPad.jpg"];
        } else {
            bg = [SKSpriteNode spriteNodeWithImageNamed:@"BG.jpg"];
        }
        [bg setAnchorPoint:CGPointMake(0, 0)];
        [bg setSize:size];
        bg.alpha = 0.0f;
        
        SKAction *fadeAction = [SKAction fadeInWithDuration:4.0];
        [bg runAction:fadeAction];
        
        [self addNode:bg atWorldLayer:SDIWorldLayerGround];
        [self buildWorld];
        
        sSharedBGEmitter = [SKEmitterNode sdi_emitterNodeWithEmitterNamed:@"MeteorShower"];
        [sSharedBGEmitter setParticlePosition:CGPointMake(self.frame.size.width,self.frame.size.height)];
        [self addNode:sSharedBGEmitter atWorldLayer:SDIWorldLayerGround];
    }
    return self;
}

- (void)runIntroWithSize:(CGSize)size {
    // Intro
    SKSpriteNode *introImage = nil;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        introImage = [SKSpriteNode spriteNodeWithImageNamed:@"AV-Logo-iPad.png"];
    } else {
        introImage = [SKSpriteNode spriteNodeWithImageNamed:@"AV-Logo.png"];
    }
    
    [introImage setAnchorPoint:CGPointMake(0, 0)];
    [introImage setSize:size];
    
    SKAction *wait = [SKAction waitForDuration:3.0];
    SKAction *fadeOutAction = [SKAction fadeOutWithDuration:1.0];
    
    [introImage runAction:[SKAction sequence:@[wait, fadeOutAction]]];
    
    [self addNode:introImage atWorldLayer:SDIWorldLayerTop];
}

#pragma mark - World Building
- (void)buildWorld
{
    NSLog(@"Building the world");
    
    // Configure physics for the world.
    self.physicsWorld.gravity = CGVectorMake(0.0f, 0.0f); // no gravity
    self.physicsWorld.contactDelegate = self;
}

#pragma mark - Level Start
- (void)startGame
{
    [self reset];
    
    // The game has begun!
    self.started = YES;
    self.gameOver = NO;
    // Set the BG particle emitter to end after 5 more particles.
    [sSharedBGEmitter setNumParticlesToEmit:5];
    
    [self buildHUD];
    
    // Phase 1 - Testing:
    // Create ship
    [self addShip];
    
    // TODO: remove insctructions.
    _instuctionLabel = [SKLabelNode labelNodeWithFontNamed:FontNamedCopperplate];
    _instuctionLabel.text = @"Swipe anywhere to create an asteroid.";
    _instuctionLabel.fontSize = 14;
    _instuctionLabel.position = CGPointMake(CGRectGetMidX(self.frame),
                                            CGRectGetMidY(self.frame) + 50);
    _instuctionLabel.alpha = 0.0f;
    SKAction *fadeAction = [SKAction fadeInWithDuration:1.0];
    [_instuctionLabel runAction:fadeAction];
    [self addNode:_instuctionLabel atWorldLayer:SDIWorldLayerBelowCharacter];
    
    // Schedule UFOs
    SKAction *wait = [SKAction waitForDuration:3];
    SKAction *createUFO = [SKAction runBlock:^{
        [self addUFOAtPosition:CGPointMake(CGRectGetMidX(self.frame),
                                           CGRectGetMidY(self.frame))];
    }];
    SKAction *removeUFO = [SKAction runBlock:^{
        SDIUFOCharacter *tmpUFO = [self.ufos lastObject];
        [self.ufos removeObject:tmpUFO];
        [tmpUFO removeFromParent];
    }];
    SKAction *updateUFO = [SKAction sequence:@[wait, createUFO, wait, removeUFO]];
    [self runAction:[SKAction repeatActionForever:updateUFO]];
}

- (void)reset
{
    self.pointsScored = 0;
    
    // Clear everything from previous game.
    for (SDIShip *ship in self.ships) {
        [ship removeFromParent];
    }
    
    for (SDIAsteroid *asteroid in self.asteroids) {
        [asteroid removeFromParent];
    }
    
    for (SDIUFOCharacter *ufo in self.ufos) {
        [ufo removeFromParent];
    }
    
    for (SKSpriteNode *shipLife in self.shipLives) {
        [shipLife removeFromParent];
    }
    
    for (SKSpriteNode *asteroidLife in self.asteroidLives) {
        [asteroidLife removeFromParent];
    }
    
    [self.ships removeAllObjects];
    [self.asteroids removeAllObjects];
    [self.ufos removeAllObjects];
    [self.shipLives removeAllObjects];
    [self.asteroidLives removeAllObjects];
    
//    [self removeAllActions];
}

- (void)pauseGame
{
    // Don't do anything if the game is over.
    if (self.isGameOver) return;
    
    // Pause/Un-Pause
    self.gamePaused = !self.isGamePaused;
    
    // If the game is paused
    if (self.gamePaused) {
        [self pause];
    }
    // If the game is un-paused
    else {
        [self unPause];
    }
}

- (void)pause
{
    NSLog(@"pause");
    self.pausedLabel = [SKLabelNode labelNodeWithFontNamed:FontNamedCopperplate];
    [self.pausedLabel setName:@"pausedLabel"];
    self.pausedLabel.text = @"Paused!";
    self.pausedLabel.fontSize = 40;
    
    self.pausedLabel.position = CGPointMake(CGRectGetMidX(self.frame),
                                            CGRectGetMidY(self.frame) + 20);
    self.pausedLabel.alpha = 0.0;
    [self addNode:self.pausedLabel atWorldLayer:SDIWorldLayerAboveCharacter];
    
//    _mainMenuLabel = [SKLabelNode labelNodeWithFontNamed:FontNamedCopperplate];
//    [_mainMenuLabel setName:@"mainMenuLabel-pause"];
//    [_mainMenuLabel setText:@"Main Menu"];
//    [_mainMenuLabel setAlpha:0];
//    [_mainMenuLabel setScale:1.0f];
//    _mainMenuLabel.position = CGPointMake(CGRectGetMidX(self.frame),
//                                          CGRectGetMidY(self.frame) - 50);
//    [self addNode:_mainMenuLabel atWorldLayer:SDIWorldLayerTop];
    
    SKAction *moveDown = [SKAction moveByX:0 y:-20 duration:0.1];
    SKAction *fadeIn = [SKAction fadeInWithDuration:0.1];
    SKAction *sound = [SKAction playSoundFileNamed:@"Pause.mp3" waitForCompletion:NO];
    
    SKAction *group = [SKAction group:@[moveDown, fadeIn, sound]];
    
//    [_mainMenuLabel runAction:group];
    
    [self.pausedLabel runAction:group completion:^{
        [self.view setPaused:self.isGamePaused];
    }];
}

- (void)unPause
{
    NSLog(@"un-pause");
    if (self.pausedLabel != nil) {
        [self.view setPaused:self.isGamePaused];
        self.pausedLabel.alpha = 1.0f;
        
        SKAction *moveDown = [SKAction moveByX:0 y:-10 duration:0.2];
        SKAction *scale = [SKAction scaleTo:0 duration:0.2];
        SKAction *fadeOut = [SKAction fadeOutWithDuration:0.2];
        SKAction *sound = [SKAction playSoundFileNamed:@"Unpause.mp3" waitForCompletion:NO];
        
        SKAction *group = [SKAction group:@[moveDown, scale, fadeOut, sound]];
        
//        [_mainMenuLabel runAction:group];
        
        [self.pausedLabel runAction:group completion:^{
            [self.pausedLabel removeFromParent];
            self.pausedLabel = nil;
        }];
    }
}

- (void)endGameWithWin:(BOOL)win
{
    NSLog(@"game over");
    self.gameOver = YES;
    
    _scoredPointsLabel = [SKLabelNode labelNodeWithFontNamed:FontNamedCopperplate];
    [_scoredPointsLabel setName:@"scoredPointsLabel"];
    [_scoredPointsLabel setAlpha:0];
    [_scoredPointsLabel setScale:1.0f];
    _scoredPointsLabel.position = CGPointMake(CGRectGetMidX(self.frame),
                                              CGRectGetMidY(self.frame) - 75);
    [self addNode:_scoredPointsLabel atWorldLayer:SDIWorldLayerTop];
    
    _endGameLabel = [SKLabelNode labelNodeWithFontNamed:FontNamedCopperplate];
    [_endGameLabel setName:@"endGameLabel"];
    [_endGameLabel setAlpha:0];
    [_endGameLabel setScale:2.0f];
    _endGameLabel.position = CGPointMake(CGRectGetMidX(self.frame),
                                         CGRectGetMidY(self.frame));
    [self addNode:_endGameLabel atWorldLayer:SDIWorldLayerTop];
    
    _mainMenuLabel = [SKLabelNode labelNodeWithFontNamed:FontNamedCopperplate];
    [_mainMenuLabel setName:@"mainMenuLabel"];
    [_mainMenuLabel setText:@"Main Menu"];
    [_mainMenuLabel setAlpha:0];
    [_mainMenuLabel setScale:2.0f];
    _mainMenuLabel.position = CGPointMake(CGRectGetMidX(self.frame),
                                         CGRectGetMidY(self.frame) + 75);
    [self addNode:_mainMenuLabel atWorldLayer:SDIWorldLayerTop];
    
    // Win/Loss alternatives
    if (win) {
        [_endGameLabel setText:@"Smashed it!"];
        
        // Count asteroids left on the field.
        for (SDIAsteroid *asteroid in self.asteroids) {
            if (CGSizeEqualToSize(asteroid.size, kLargeAsteroidSize)) {
                self.pointsScored += kLargeAsteroidPoints;
            } else if (CGSizeEqualToSize(asteroid.size, kMediumAsteroidSize)) {
                self.pointsScored += kMediumAsteroidPoints;
            } else if (CGSizeEqualToSize(asteroid.size, kSmallAsteroidSize)) {
                self.pointsScored += kSmallAsteroidPoints;
            }
        }
        
        // Count asteroid lives left in the hud
        for (SDIAsteroid *asteroid in self.asteroidLives) {
            self.pointsScored += kLargeAsteroidPoints;
        }
        
        NSLog(@"total points scored: %d", self.pointsScored);
        [_scoredPointsLabel setText:[NSString stringWithFormat:@"*** %d Points ***", self.pointsScored]];
        
        NSNumber *score = [[NSUserDefaults standardUserDefaults] objectForKey:kTotalScoreForPlayer];
        if (score == nil) {
            score = [NSNumber numberWithInt:0];
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:score.intValue + self.pointsScored] forKey:kTotalScoreForPlayer];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [_playerTotalScoreLabel setText:[NSString stringWithFormat:@"%d", score.intValue + self.pointsScored]];
        
        // Update gamecenter
        [[SDILeaderBoardManager shared] reportHighScore:self.pointsScored forLeaderboardIdentifier:kHighScoreId];
        [[SDILeaderBoardManager shared] reportHighScore:score.intValue + self.pointsScored forLeaderboardIdentifier:kTotalScoreId];
        
        // Update achievements
        [[SDILeaderBoardManager shared] reportAchievementIdentifier:kDestroyAllAchievementId percentComplete:100];
        if (self.pointsScored >= 50) {
            [[SDILeaderBoardManager shared] reportAchievementIdentifier:k50PointsAchievementId percentComplete:100];
        } else if (self.pointsScored >= 100) {
            [[SDILeaderBoardManager shared] reportAchievementIdentifier:k100PointsAchievementId percentComplete:100];
        } else if (self.pointsScored >= 200) {
            [[SDILeaderBoardManager shared] reportAchievementIdentifier:k200PointsAchievementId percentComplete:100];
        }
        
    } else {
        [_endGameLabel setText:@"Better luck next time."];
        
        // Update achievements
        if (self.shipLives.count == kMaxNumberOfShipLives) {
            [[SDILeaderBoardManager shared] reportAchievementIdentifier:kDestroyNoneAchievementId percentComplete:100];
        }
    }
    
    _playAgainButton = [SKSpriteNode spriteNodeWithImageNamed:@"30-circle-play.png"];
    [_playAgainButton setName:@"playAgainButton"];
    _playAgainButton.position = CGPointMake(CGRectGetMidX(self.frame),
                                           CGRectGetMidY(self.frame) - 25);
    [_playAgainButton setAlpha:0];
    [self addNode:_playAgainButton atWorldLayer:SDIWorldLayerTop];
    
    // Group actions
    SKAction *fadeIn = [SKAction fadeInWithDuration:1];
    SKAction *scale = [SKAction scaleTo:1 duration:1];
    
    SKAction *group = [SKAction group:@[fadeIn, scale]];
    
    [_scoredPointsLabel runAction:group];
    [_endGameLabel runAction:group];
    [_playAgainButton runAction:group];
    [_mainMenuLabel runAction:group];
    
//    SKAction *sound = [SKAction playSoundFileNamed:@".mp3" waitForCompletion:NO];
}

#pragma mark - HUD and Scores
- (void)buildHUD
{
    // Add Pause button
    SKSpriteNode *pauseButton = [SKSpriteNode spriteNodeWithImageNamed:@"29-circle-pause.png"];
    [pauseButton setName:@"pauseButton"];
    [pauseButton setSize:CGSizeMake(pauseButton.frame.size.width * 1.25, pauseButton.frame.size.height * 1.25)];
    [pauseButton setAnchorPoint:CGPointMake(1, 1)];
    pauseButton.position = CGPointMake(self.size.width - 5, self.size.height - 5);
    [self addNode:pauseButton atWorldLayer:SDIWorldLayerTop];
    
    // Add ship lives
    for (int i = 0; i < kMaxNumberOfShipLives; i++) {
        SKSpriteNode *shipLife = [SKSpriteNode spriteNodeWithImageNamed:@"Spaceship.png"];
        [shipLife setName:@"shipLife"];
        [shipLife setSize:kLifeShipSize];
        [shipLife setAnchorPoint:CGPointMake(0, 1)];
        shipLife.position = CGPointMake(5 + (i * (shipLife.frame.size.width + 3)), self.size.height - 5);
        [[self shipLives] addObject:shipLife];
        [self addNode:shipLife atWorldLayer:SDIWorldLayerTop];
    }
    
    // Add asteroid lives
    for (int i = 0; i < kMaxNumberOfAsteroidLives; i++) {
        SKSpriteNode *asteroidLife = [SKSpriteNode spriteNodeWithImageNamed:@"Asteroid.png"];
        [asteroidLife setName:@"asteroidLife"];
        [asteroidLife setSize:kLifeAsteroidSize];
        [asteroidLife setAnchorPoint:CGPointMake(1, 1)];
        asteroidLife.position = CGPointMake(pauseButton.frame.origin.x - 10 - (i * (asteroidLife.frame.size.width + 3)), self.size.height - 5);
        [[self asteroidLives] addObject:asteroidLife];
        [self addNode:asteroidLife atWorldLayer:SDIWorldLayerTop];
    }
    
    // Add player total score
    NSNumber *score = [[NSUserDefaults standardUserDefaults] objectForKey:kTotalScoreForPlayer];
    if (score == nil) {
        score = [NSNumber numberWithInt:0];
    }
    
    // Add the total score label
    if (CGPointEqualToPoint(_playerTotalScoreLabel.position, CGPointZero)) {
        _playerTotalScoreLabel = [SKLabelNode labelNodeWithFontNamed:FontNamedCopperplate];
        _playerTotalScoreLabel.position = CGPointMake(CGRectGetMidX(self.frame), 0);
        [self addNode:_playerTotalScoreLabel atWorldLayer:SDIWorldLayerTop];
    }
    [_playerTotalScoreLabel setName:@"playerTotalScoreLabel"];
    [_playerTotalScoreLabel setScale:0.5f];
    [_playerTotalScoreLabel setText:[NSString stringWithFormat:@"%d", score.intValue]];
}

#pragma mark - Loop Update
- (void)updateWithTimeSinceLastUpdate:(CFTimeInterval)timeSinceLast
{
    // Update all ship
    for (SDIShip *ship in self.ships) {
        [ship updateWithTimeSinceLastUpdate:timeSinceLast];
    }
    
    // Update the asteroids.
    for (SDIAsteroid *asteroid in self.asteroids) {
        [asteroid updateWithTimeSinceLastUpdate:timeSinceLast];
    }
    
    // Update the UFOs
    for (SDIUFOCharacter *ufo in self.ufos) {
        [ufo updateWithTimeSinceLastUpdate:timeSinceLast];
    }
}

#pragma mark - Physics Delegate
- (void)didBeginContact:(SKPhysicsContact *)contact
{
    // Either bodyA or bodyB in the collision could be a character.
    SKNode *node = contact.bodyA.node;
    if ([node isKindOfClass:[SDICharacter class]]) {
        [(SDICharacter *)node collidedWith:contact.bodyB];
    }
    
    // Check bodyB too.
    node = contact.bodyB.node;
    if ([node isKindOfClass:[SDICharacter class]]) {
        [(SDICharacter *)node collidedWith:contact.bodyA];
    }
    
    // Handle collisions with projectiles.
    if (contact.bodyA.categoryBitMask & SDIColliderTypeProjectile || contact.bodyB.categoryBitMask & SDIColliderTypeProjectile) {
        SKNode *projectile = (contact.bodyA.categoryBitMask & SDIColliderTypeProjectile) ? contact.bodyA.node : contact.bodyB.node;
        
        [projectile runAction:[SKAction removeFromParent]];
        
        // Build up a "one shot" particle to indicate where the projectile hit.
//        SKEmitterNode *emitter = [[self sharedProjectileSparkEmitter] copy];
//        [self addNode:emitter atWorldLayer:SDIWorldLayerAboveCharacter];
//        emitter.position = projectile.position;
//        SDIRunOneShotEmitter(emitter, 0.15f);
    }
}

#pragma mark - Event Handling - iOS
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    /* Called when a touch begins */
    for (UITouch *touch in touches) {
        _gestureStartPoint = [touch locationInNode:self];
        NSLog(@"_gestureStartPoint x:%f y:%f", _gestureStartPoint.x, _gestureStartPoint.y);
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    // Return if the game has not been started;
    if (!self.isStarted) return;
    
    // Get the touch location
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    
    // If pause button touched then stop everything.
    if ([node.name isEqualToString:@"pauseButton"]) {
        [self pauseGame];
        return;
    }
    
    // If play button touched then start a new game.
    if ([node.name isEqualToString:@"playAgainButton"]) {
        [self startGame];
        [self removeEndGameElements];
        return;
    }
    
    // If main menu button is touched.
    if ([node.name isEqualToString:@"mainMenuLabel"]) {
        [self removeEndGameElements];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"mainMenuNotificaton" object:nil];
        return;
    }
    
//    // If main menu button is touched.
//    if ([node.name isEqualToString:@"mainMenuLabel-pause"]) {
////        [self unPause];
//        
//        [self.pausedLabel removeFromParent];
//        self.pausedLabel = nil;
//        
//        [self reset];
//        
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"mainMenuNotificaton" object:nil];
//        return;
//    }
    
    // If the game is paused then don't do anything beyond this point.
    if (self.isGamePaused) return;
    
    if (_instuctionLabel != nil) {
        SKAction *fadeOutAction = [SKAction fadeOutWithDuration:1.0];
        SKAction *fadeInAction = [SKAction fadeInWithDuration:1.0];
        SKAction *wait = [SKAction waitForDuration:6.0f];
        SKAction *updateText = [SKAction runBlock:^{
            _instuctionLabel.text = @"Destroy all of the enemy ships to win the game!";
        }];
        SKAction *removeFromView = [SKAction runBlock:^{
            [_instuctionLabel removeFromParent];
            _instuctionLabel = nil;
        }];
        
        [_instuctionLabel runAction:[SKAction sequence:@[fadeOutAction, updateText, fadeInAction, wait, fadeOutAction, removeFromView]]];
    }
    
    // If there are still asteroid lives left then create another asteroid.
    if ([[self asteroidLives] count] > 0) {
        SKSpriteNode *asteroidLife = [[self asteroidLives] lastObject];
        if (asteroidLife != nil) {
            [[self asteroidLives] removeObject:asteroidLife];
            [asteroidLife removeFromParent];
            
            // Swipe to create asteroid
            CGPoint gestureEndPoint = [touch locationInNode:self];
            NSLog(@"gestureEndPoint x:%f y:%f", gestureEndPoint.x, gestureEndPoint.y);
            float x = _gestureStartPoint.x - gestureEndPoint.x;
            float y = _gestureStartPoint.y - gestureEndPoint.y;
            // Create asteroids
            [self addAsteroidAtPosition:_gestureStartPoint
                               withSize:kLargeAsteroidSize
                      withMoveDirection:CGPointMake(x*-1, y*-1)];
        }
    }
}

- (void)removeEndGameElements
{
    // Fade out the end game label;
    SKAction *fadeOut = [SKAction fadeOutWithDuration:.5];
    SKAction *block = [SKAction runBlock:^{
        [_endGameLabel removeFromParent];
    }];
    SKAction *sequence = [SKAction sequence:@[fadeOut, block]];
    [_endGameLabel runAction:sequence];
    
    // Fade play again button
    block = [SKAction runBlock:^{
        [_playAgainButton removeFromParent];
    }];
    sequence = [SKAction sequence:@[fadeOut, block]];
    [_playAgainButton runAction:sequence];
    
    // Fade score points label.
    block = [SKAction runBlock:^{
        [_scoredPointsLabel removeFromParent];
    }];
    sequence = [SKAction sequence:@[fadeOut, block]];
    [_scoredPointsLabel runAction:sequence];
    
    // Fade score points label.
    block = [SKAction runBlock:^{
        [_mainMenuLabel removeFromParent];
    }];
    sequence = [SKAction sequence:@[fadeOut, block]];
    [_mainMenuLabel runAction:sequence];
}

#pragma mark - Shared Assets
+ (void)loadSceneAssets
{
    // Load archived emitters
    sSharedSpawnEmitter = [SKEmitterNode sdi_emitterNodeWithEmitterNamed:@"Warp"];
    
    sSharedSpawnEmitter2 = [SKEmitterNode sdi_emitterNodeWithEmitterNamed:@"WarpLightning"];
    
    // Load assets for all the sprites within this scene.
    [SDIShip loadSharedAssets];
    [SDIAsteroid loadSharedAssets];
    [SDIUFOCharacter loadSharedAssets];
}

static SKEmitterNode *sSharedSpawnEmitter = nil;
- (SKEmitterNode *)sharedSpawnEmitter
{
    return sSharedSpawnEmitter;
}

static SKEmitterNode *sSharedSpawnEmitter2 = nil;
- (SKEmitterNode *)sharedSpawnEmitter2
{
    return sSharedSpawnEmitter2;
}

static SKEmitterNode *sSharedBGEmitter = nil;
- (SKEmitterNode *)sharedBGEmitter
{
    return sSharedBGEmitter;
}


@end

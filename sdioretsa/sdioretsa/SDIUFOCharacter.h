//
//  SDIUFOCharacter.h
//  sdioretsa
//
//  Created by Will Saults on 12/12/13.
//  Copyright (c) 2013 AppVentures LLC. All rights reserved.
//

#import "SDICharacter.h"

#define kUFOSize (CGSizeMake(75, 47))

@interface SDIUFOCharacter : SDICharacter

- (id)initAtPosition:(CGPoint)position;

@end

//
//  SDILeaderBoardManager.h
//  sdioretsa
//
//  Created by Will Saults on 1/23/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GameKit/GameKit.h>

// Leaderboard
#define kTotalScoreForPlayer @"totalScoreForPlayer"
#define kHighScoreId @"sdioretsa.high.score"
#define kTotalScoreId @"sdioretsa.total.points.earned"

// Achievements
#define kLuckySevenAchievementId @"lucky.seven"
#define k50PointsAchievementId @"50.points"
#define k100PointsAchievementId @"100.points"
#define k200PointsAchievementId @"200.points"
#define kDestroyAllAchievementId @"destroy.all.ships"
#define kDestroyNoneAchievementId @"Destroy.no.ships"

@interface SDILeaderBoardManager : NSObject

+(SDILeaderBoardManager *)shared;

- (void)reportHighScore:(NSInteger)highScore forLeaderboardIdentifier:(NSString*)identifier;
- (void)reportAchievementIdentifier:(NSString*)identifier percentComplete:(float)percent;
- (void)loadAchievements;

@end

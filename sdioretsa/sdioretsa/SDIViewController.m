//
//  SDIViewController.m
//  sdioretsa
//
//  Created by Will Saults on 11/25/13.
//  Copyright (c) 2013 AppVentures LLC. All rights reserved.
//

#import "SDIViewController.h"
#import "SDIGameplayScene.h"
#import "SDILeaderBoardManager.h"

// Uncomment this line to show debug info in the Sprite Kit view:
#define SHOW_DEBUG_INFO

@interface SDIViewController()

@property (nonatomic, strong) SKView *skView;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UIButton *playButton;
@property (nonatomic, weak) IBOutlet UIButton *localLeaderboardButton;
@property (nonatomic, weak) IBOutlet UIButton *gameCenterButton;
@property (nonatomic, weak) IBOutlet UIButton *creditsButton;
@property (nonatomic, weak) IBOutlet UIButton *creditsImage;
@property (nonatomic) SDIGameplayScene *scene;

@end

@implementation SDIViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showUIElements) name:@"mainMenuNotificaton" object:nil];
    
    [self authenticateLocalPlayer];
    
    // Load the shared assets of the scene before we initialize and load it.
    [SDIGameplayScene loadSceneAssetsWithCompletionHandler:^{
        
        // Configure the view.
        CGSize viewSize = self.view.bounds.size;
        _skView = (SKView *)self.view;
        
        // Create and configure the scene.
        SDIGameplayScene *gamePlayScene = [[SDIGameplayScene alloc] initWithSize:viewSize];
        gamePlayScene.scaleMode = SKSceneScaleModeAspectFill;
        self.scene = gamePlayScene;
        // Present the scene.
        [_skView presentScene:gamePlayScene];
        
        [self hideUIElements:NO delay:5];
    }];

    
#ifdef SHOW_DEBUG_INFO
    // Show debug information.
    _skView.showsFPS = YES;
    _skView.showsDrawCount = YES;
    _skView.showsNodeCount = YES;
#endif
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscape;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - UI Display and Actions
- (void)showUIElements
{
    [self hideUIElements:NO delay:0];
}

- (void)hideUIElements:(BOOL)shouldHide delay:(NSTimeInterval)delay
{
    CGFloat alpha = shouldHide ? 0.0f : 1.0f;
//    NSTimeInterval delay = shouldHide ? 0.0f : 5.0f;
    BOOL shouldAnimate = YES;
    
    if (shouldAnimate) {
        [UIView animateWithDuration:.4 delay:delay options:UIViewAnimationOptionCurveEaseInOut animations:^{
            [self.titleLabel setAlpha:alpha];
            [self.playButton setAlpha:alpha];
            [self.creditsButton setAlpha:alpha];
            [self.gameCenterButton setAlpha:alpha];
            [self.localLeaderboardButton setAlpha:alpha];
        } completion:nil];
    } else {
        [self.titleLabel setAlpha:alpha];
        [self.playButton setAlpha:alpha];
        [self.creditsButton setAlpha:alpha];
        [self.gameCenterButton setAlpha:alpha];
        [self.localLeaderboardButton setAlpha:alpha];
    }
}

- (IBAction)play:(id)sender
{
    [self startGame];
}

- (IBAction)credits:(id)sender
{
    CGFloat alpha = self.creditsImage.alpha == 1.0f ? 0.0f : 1.0f;
    // Toggle the credits button image.
    [UIView animateWithDuration:.4 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [self.creditsImage setAlpha:alpha];
    } completion:nil];
}

- (IBAction)gameCenter:(id)sender
{
    [self showGameCenter];
}

#pragma mark - Starting the Game
- (void)startGame {
    [self hideUIElements:YES delay:0];
    [self.scene startGame];
}

#pragma mark - Gamecenter
- (void)authenticateLocalPlayer
{
    __weak GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    localPlayer.authenticateHandler = ^(UIViewController *viewController, NSError *error){
        if (viewController != nil)
        {
            //showAuthenticationDialogWhenReasonable: is an example method name. Create your own method that displays an authentication view when appropriate for your app.
//            [self showAuthenticationDialogWhenReasonable: viewController];
        }
        else if (localPlayer.isAuthenticated)
        {
            // Called after the loacal player is authenticated.
            [self authenticatedPlayer:localPlayer];
            [[SDILeaderBoardManager shared] loadAchievements];
        }
        else
        {
//            [self disableGameCenter];
        }
    };
}

- (void)authenticatedPlayer:(GKLocalPlayer *)localPlayer
{
    NSLog(@"authenticatedPlayer");
    
    NSNumber *score = [[NSUserDefaults standardUserDefaults] objectForKey:kTotalScoreForPlayer];
    if (score == nil) {
        score = [NSNumber numberWithInt:0];
    }
    
    // Update gamecenter
    [[SDILeaderBoardManager shared] reportHighScore:score.integerValue forLeaderboardIdentifier:kTotalScoreId];
}

- (void)showGameCenter
{
    GKGameCenterViewController *gameCenterController = [[GKGameCenterViewController alloc] init];
    if (gameCenterController != nil)
    {
        gameCenterController.gameCenterDelegate = self;
        [self presentViewController: gameCenterController animated: YES completion:nil];
    }
}

- (void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end

//
//  SDIShip.m
//  sdioretsa
//
//  Created by Will Saults on 11/25/13.
//  Copyright (c) 2013 AppVentures LLC. All rights reserved.
//

#import "SDIShip.h"
#import "SDIMultiLayerScene.h"
#import "SDIUtilities.h"
#import "SDIEvadeAndAttackAI.h"

#define kShipProjectileSpeed 480.0
#define kShipProjectileLifetime 1.0 // 1.0 seconds until the projectile disappears
#define kShipProjectileFadeOutTime 0.6 // 0.6 seconds until the projectile starts to fade out

#define kSpriteName @"Spaceship"
#define kDefaultSize (CGSizeMake(30, 30))
#define kPointValue 500
#define kShipCollisionRadius 25
#define kAsteroidAlertRadius (kShipCollisionRadius * 10)
#define kProjectileFireDelay .35

@interface SDIShip ()

@property (nonatomic) CGFloat totalInterval;

@end

@implementation SDIShip

- (id)initAtPosition:(CGPoint)position
{
    self = [super initWithName:kSpriteName AtPosition:position withSize:kDefaultSize];
    if (self) {
        NSLog(@"created ship %@", self);
        self.attacking = NO;
        self.pointValue = kPointValue;
        
        // Make it AWARE!
        SDIEvadeAndAttackAI *ai = [[SDIEvadeAndAttackAI alloc] initWithCharacter:self target:nil];
        ai.dodgeRadius = kAsteroidAlertRadius;
        ai.maxAlertRadius = kAsteroidAlertRadius * 4.0f;
        self.intelligence = ai;
    }
    
    return self;
}

#pragma mark - Overridden Methods
- (void)configurePhysicsBody
{
    self.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:kShipCollisionRadius];
    
    // Our object type for collisions.
    self.physicsBody.categoryBitMask = SDIColliderTypeShip;
    
    // Collides with these objects.
    self.physicsBody.collisionBitMask = SDIColliderTypeAsteroid;
    
    // We want notifications for colliding with these objects.
    self.physicsBody.contactTestBitMask = SDIColliderTypeAsteroid;
}

/* Overridden Methods. */
- (void)animationDidComplete:(SDIAnimationState)animationState
{
    switch (animationState) {
        case SDIAnimationStateDeath:{
            SDIMultiLayerScene *characterScene = [self characterScene];
            
            SKAction *sound = [SKAction playSoundFileNamed:@"ShipDeath.wav" waitForCompletion:NO];
            [self runAction:[SKAction sequence:@[sound,
                                                 [SKAction waitForDuration:0.01],
                                                 [SKAction fadeOutWithDuration:0.05f],
                                                 [SKAction runBlock:^{
                
                [[characterScene ships] removeObject:self];
            }],
                                                 [SKAction waitForDuration:1.0],
                                                 [SKAction runBlock:^{
                [self removeFromParent];
                [self removeAllActions];
                
                // If there are still ship lives left then warp in another ship.
                if ([[characterScene shipLives] count] > 0) {
                    SKSpriteNode *shipLife = [[characterScene shipLives] lastObject];
                    if (shipLife != nil) {
                        [[characterScene shipLives] removeObject:shipLife];
                        [shipLife removeFromParent];
                        [characterScene addShip];
                    }
                } else {
                    [characterScene endGameWithWin:YES];
                }
            }]]
                             ]];
            break;
        }
            
        default:
            break;
    }
}

- (void)collidedWith:(SKPhysicsBody *)other
{
    if (self.isDying) {
        return;
    }
    
    if (other.categoryBitMask & SDIColliderTypeAsteroid) {
        // Destroy the asteroid
        [self performDeath];
    }
}

#pragma mark - Shared Assets
+ (void)loadSharedAssets
{
    [super loadSharedAssets];
    
    // Load emitter nodes
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        // Projectile sound
        sSharedProjectileSoundAction = [SKAction playSoundFileNamed:@"Laser.wav" waitForCompletion:NO];
        
        // Projectile body
        sSharedProjectile = [SKSpriteNode spriteNodeWithColor:[SKColor whiteColor] size:CGSizeMake(2.0, 24.0)];
        sSharedProjectile.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:kProjectileCollisionRadius];
        sSharedProjectile.name = @"Projectile";
        sSharedProjectile.physicsBody.categoryBitMask = SDIColliderTypeProjectile;
        sSharedProjectile.physicsBody.collisionBitMask = SDIColliderTypeAsteroid;
        sSharedProjectile.physicsBody.contactTestBitMask = sSharedProjectile.physicsBody.collisionBitMask;
        
        // TODO: implement the blast, death, and damage emitter nodes.
        sSharedProjectileEmitter = [SKEmitterNode sdi_emitterNodeWithEmitterNamed:@"Blast"];
        sSharedProjectileEmitter.numParticlesToEmit = 1.0;
        
//        sSharedDeathEmitter = [SKEmitterNode sdi_emitterNodeWithEmitterNamed:@"Death"];
//        sSharedDamageEmitter = [SKEmitterNode apa_emitterNodeWithEmitterNamed:@"Damage"];
    });
}

#pragma mark - Projectiles
- (void)fireProjectile {
    if (!self.isFiring) {
        self.firing = YES;
        SDIMultiLayerScene *scene = [self characterScene];
        
        // Create projectile
        SKSpriteNode *projectile = [[self projectile] copy];
        projectile.position = self.position;
        projectile.zRotation = self.zRotation;
        
        SKEmitterNode *emitter = [[self projectileEmitter] copy];
        emitter.targetNode = [self.scene childNodeWithName:@"world"];
        [projectile addChild:emitter];
        
        [scene addNode:projectile atWorldLayer:SDIWorldLayerBelowCharacter];
        
        CGFloat rot = self.zRotation;
        
        [projectile runAction:[SKAction moveByX:-sinf(rot)*kShipProjectileSpeed*kShipProjectileLifetime
                                              y:cosf(rot)*kShipProjectileSpeed*kShipProjectileLifetime
                                       duration:kShipProjectileLifetime]];
        
        // Play sound
        [projectile runAction:[self projectileSoundAction]];
        
        // Remove projectile
        [projectile runAction:[SKAction sequence:@[[SKAction waitForDuration:kShipProjectileFadeOutTime],
                                                   [SKAction fadeOutWithDuration:kShipProjectileLifetime - kShipProjectileFadeOutTime],
                                                   [SKAction removeFromParent]]]];
    }
}

- (void)updateWithTimeSinceLastUpdate:(CFTimeInterval)interval
{
    [super updateWithTimeSinceLastUpdate:interval];
    self.totalInterval += interval;
    if (self.totalInterval > kProjectileFireDelay) {
        self.totalInterval = 0;
        self.firing = NO;
    }
}

static SKSpriteNode *sSharedProjectile = nil;
- (SKSpriteNode *)projectile {
    return sSharedProjectile;
}

static SKEmitterNode *sSharedProjectileEmitter = nil;
- (SKEmitterNode *)projectileEmitter {
    return sSharedProjectileEmitter;
}

static SKAction *sSharedProjectileSoundAction = nil;
- (SKAction *)projectileSoundAction {
    return sSharedProjectileSoundAction;
}

@end

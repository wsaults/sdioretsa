//
//  SDIConstants.h
//  sdioretsa
//
//  Created by Will Saults on 12/11/13.
//  Copyright (c) 2013 AppVentures LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SDIConstants : NSObject

extern NSString * const FontNamedCopperplate;

@end

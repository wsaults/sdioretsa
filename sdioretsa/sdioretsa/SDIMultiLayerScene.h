//
//  SDIMultiLayerScene.h
//  sdioretsa
//
//  Created by Will Saults on 11/25/13.
//  Copyright (c) 2013 AppVentures LLC. All rights reserved.
//

/* The layers in a scene. */
typedef enum : uint8_t {
    SDIWorldLayerGround = 0,
	SDIWorldLayerBelowCharacter,
	SDIWorldLayerCharacter,
	SDIWorldLayerAboveCharacter,
	SDIWorldLayerTop,
	kWorldLayerCount
} SDIWorldLayer;

#import <SpriteKit/SpriteKit.h>

#define kMinTimeInterval (1.0f / 60.0f)
#define kMaxNumberOfShipLives 3
#define kMaxNumberOfAsteroidLives 3

@class SDIShip;
@class SDIAsteroid;
@class SDIUFOCharacter;

/* Completion handler for callback after loading assets asynchronously. */
typedef void (^SDIAssetLoadCompletionHandler)(void);

@interface SDIMultiLayerScene : SKScene

@property (nonatomic, readonly) SKNode *world;                    // Root node to which all game renderables are attached
@property (nonatomic, strong) NSMutableArray *asteroids;          // All active asteroids in the game
@property (nonatomic, strong) NSMutableArray *ships;              // All active ships in the game
@property (nonatomic, strong) NSMutableArray *ufos;               // All active UFOs in the game
@property (nonatomic, strong) NSMutableArray *shipLives;          // Array of ship lives
@property (nonatomic, strong) NSMutableArray *asteroidLives;      // Array of asteroid lives
@property (nonatomic) int pointsScored;                           // A counter for points scored at the end of the game.

/* Start loading all the shared assets for the scene in the background. This method calls +loadSceneAssets
 on a background queue, then calls the callback handler on the main thread. */
+ (void)loadSceneAssetsWithCompletionHandler:(SDIAssetLoadCompletionHandler)callback;

/* Overridden by subclasses to load scene-specific assets. */
+ (void)loadSceneAssets;

/* Overridden by subclasses to release assets used only by this scene. */
+ (void)releaseSceneAssets;

/* Overridden by subclasses to provide an emitter */
- (SKEmitterNode *)sharedSpawnEmitter;
- (SKEmitterNode *)sharedSpawnEmitter2;
- (SKEmitterNode *)sharedBGEmitter;

/* Overridden by subclasses to update the scene - called once per frame. */
- (void)updateWithTimeSinceLastUpdate:(NSTimeInterval)timeSinceLast;

/* All sprites in the scene should be added through this method to ensure they are placed in the correct world layer. */
- (void)addNode:(SKNode *)node atWorldLayer:(SDIWorldLayer)layer;

/* Adds to that player's score. */
- (void)addToScore:(uint32_t)amount;

/* Ship */
- (SDIShip *)addShip;
- (void)shipWasKilled;
- (SDIAsteroid *)addAsteroidAtPosition:(CGPoint)position withSize:(CGSize)size withMoveDirection:(CGPoint)moveDirection;
- (SDIUFOCharacter *)addUFOAtPosition:(CGPoint)position;

-(void)wrapSpriteIfNeeded:(SKSpriteNode *)node;

/* Overridden by subclasses */
- (void)endGameWithWin:(BOOL)win;

@end

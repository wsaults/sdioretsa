//
//  SDIAsteroid.h
//  sdioretsa
//
//  Created by Will Saults on 11/25/13.
//  Copyright (c) 2013 AppVentures LLC. All rights reserved.
//

#import "SDICharacter.h"

#define kLargeAsteroidSize (CGSizeMake(50, 50))
#define kMediumAsteroidSize (CGSizeMake(25, 25))
#define kSmallAsteroidSize (CGSizeMake(15, 15))
#define kLifeAsteroidSize (CGSizeMake(10, 10))

#define kLargeAsteroidPoints 100
#define kMediumAsteroidPoints 50
#define kSmallAsteroidPoints 25

@interface SDIAsteroid : SDICharacter

- (id)initAtPosition:(CGPoint)position withSize:(CGSize)size withMoveDirection:(CGPoint)moveDirection;

@end

//
//  SDICharacter.h
//  sdioretsa
//
//  Created by Will Saults on 11/25/13.
//  Copyright (c) 2013 AppVentures LLC. All rights reserved.
//

/* Used by the move: method to move a character in a given direction. */
typedef enum : uint8_t {
    APAMoveDirectionForward = 0,
    APAMoveDirectionLeft,
    APAMoveDirectionRight,
    APAMoveDirectionBack,
} SDIMoveDirection;

/* The different animation states of an animated character. */
typedef enum : uint8_t {
    SDIAnimationStateIdle = 0,
    SDIAnimationStateMove,
    SDIAnimationStateAttack,
    SDIAnimationStateGetHit,
    SDIAnimationStateDeath,
    kAnimationStateCount
} SDIAnimationState;

/* Bitmask for the different entities with physics bodies. */
typedef enum : uint8_t {
    SDIColliderTypeShip             = 1,
    SDIColliderTypeAsteroid         = 2,
    SDIColliderTypeProjectile       = 4,
} SDIColliderType;

#define kMovementSpeed 200.0
#define kRotationSpeed 0.06

#define kCharacterCollisionRadius   40
#define kProjectileCollisionRadius  10

#import <SpriteKit/SpriteKit.h>

@class SDIMultiLayerScene;

@interface SDICharacter : SKSpriteNode

@property (nonatomic, getter=isFiring) BOOL firing;             // Check if the character is firing
@property (nonatomic, getter=isDying) BOOL dying;               // Check if the character is dying
@property (nonatomic, getter=isAttacking) BOOL attacking;       // Check if the character is attacking
@property (nonatomic) CGFloat health;                           // Health counter
@property (nonatomic, getter=isAnimated) BOOL animated;         // Determine if the character is animated.
@property (nonatomic) CGFloat animationSpeed;                   
@property (nonatomic) CGFloat movementSpeed;
@property (nonatomic) int pointValue;
@property (nonatomic) CGPoint moveDirection;

@property (nonatomic) NSString *activeAnimationKey;
@property (nonatomic) SDIAnimationState requestedAnimation;

/* Preload shared animation frames, emitters, etc. */
+ (void)loadSharedAssets;

/* Initialize a standard sprite. */
- (id)initWithName:(NSString *)spriteName AtPosition:(CGPoint)position withSize:(CGSize)size;

/* Initialize a standard sprite with texture. */
- (id)initWithTexture:(SKTexture *)texture atPosition:(CGPoint)position;

/* Reset a character for reuse. */
- (void)reset;

/* Overridden Methods. */
- (void)animationDidComplete:(SDIAnimationState)animation;
- (void)collidedWith:(SKPhysicsBody *)other;
- (void)performDeath;
- (void)configurePhysicsBody;

/* Assets - should be overridden for animated characters. */
- (NSArray *)moveAnimationFrames;

/* Loop Update - called once per frame. */
- (void)updateWithTimeSinceLastUpdate:(CFTimeInterval)interval;

/* Orientation, Movement, and Attacking. */
- (void)move:(SDIMoveDirection)direction withTimeInterval:(NSTimeInterval)timeInterval;
- (CGFloat)faceTo:(CGPoint)position withDuration:(CGFloat)duration;
- (void)evade:(CGPoint)direction withTimeInterval:(NSTimeInterval)timeInterval;
- (void)moveTowards:(CGPoint)position withTimeInterval:(NSTimeInterval)timeInterval;
- (void)moveInDirection:(CGPoint)direction withTimeInterval:(NSTimeInterval)timeInterval;

/* Scenes. */
- (void)addToScene:(SDIMultiLayerScene *)scene; // also adds the shadow blob
- (SDIMultiLayerScene *)characterScene; // returns the SDIMultiLayerScene this character is in

/* Animation. */
- (void)fadeIn:(CGFloat)duration;

- (void)fireProjectile;
- (SKSpriteNode *)projectile;
- (SKEmitterNode *)projectileEmitter;

@end

//
//  SDIUtilities.h
//  sdioretsa
//
//  Created by Will Saults on 11/27/13.
//  Copyright (c) 2013 AppVentures LLC. All rights reserved.
//

/* Generate a random float between 0.0f and 1.0f. */
#define SDI_RANDOM_0_1() (arc4random() / (float)(0xffffffffu))

/* The assets are all facing Y down, so offset by pi half to get into X right facing. */
#define SDI_POLAR_ADJUST(x) x + (M_PI * 0.5f)

/* Generate a random float between two numbers */
#define boris_random(smallNumber, bigNumber) ((((float) (arc4random() % ((unsigned)RAND_MAX + 1)) / RAND_MAX) * (bigNumber - smallNumber)) + smallNumber)

/* Distance and coordinate utility functions. */
CGFloat SDIDistanceBetweenPoints(CGPoint first, CGPoint second);
CGFloat SDIRadiansBetweenPoints(CGPoint first, CGPoint second);
CGPoint SDIPointByAddingCGPoints(CGPoint first, CGPoint second);

/* Load the named frames in a texture atlas into an array of frames. */
NSArray *SDILoadFramesFromAtlas(NSString *atlasName, NSString *baseFileName, int numberOfFrames);

/* Run the given emitter once, for duration. */
void SDIRunOneShotEmitter(SKEmitterNode *emitter, CGFloat duration);

/* Run the given emitter over x number of particles. */
void SDIRunNumOfParticlesEmitter(SKEmitterNode *emitter, NSUInteger numOfParticles);


/* Category on NSValue to make it easy to access the pointValue/CGPointValue from iOS and OS X. */
@interface NSValue (SDIAdditions)
- (CGPoint)sdi_CGPointValue;
+ (instancetype)sdi_valueWithCGPoint:(CGPoint)point;
@end


/* Category on SKEmitterNode to make it easy to load an emitter from a node file created by Xcode. */
@interface SKEmitterNode (SDIAdditions)
+ (instancetype)sdi_emitterNodeWithEmitterNamed:(NSString *)emitterFileName;
@end
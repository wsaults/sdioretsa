//
//  SDIAppDelegate.h
//  sdioretsa
//
//  Created by Will Saults on 11/25/13.
//  Copyright (c) 2013 AppVentures LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SDIAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

//
//  SDILocalLeaderboardViewController.h
//  sdioretsa
//
//  Created by Will Saults on 1/29/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SDILocalLeaderboardViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@end

//
//  SDIViewController.h
//  sdioretsa
//

//  Copyright (c) 2013 AppVentures LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>
#import <GameKit/GameKit.h>

@interface SDIViewController : UIViewController <GKGameCenterControllerDelegate>

@end

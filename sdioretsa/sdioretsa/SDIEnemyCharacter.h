//
//  SDIEnemyCharacter.h
//  sdioretsa
//
//  Created by Will Saults on 12/2/13.
//  Copyright (c) 2013 AppVentures LLC. All rights reserved.
//

#import "SDICharacter.h"

@class SDIArtificialIntelligence;

@interface SDIEnemyCharacter : SDICharacter

@property (nonatomic) SDIArtificialIntelligence *intelligence;

@end

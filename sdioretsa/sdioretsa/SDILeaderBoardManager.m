//
//  SDILeaderBoardManager.m
//  sdioretsa
//
//  Created by Will Saults on 1/23/14.
//  Copyright (c) 2014 AppVentures LLC. All rights reserved.
//

#import "SDILeaderBoardManager.h"

@implementation SDILeaderBoardManager

static SDILeaderBoardManager *_shared;

+(SDILeaderBoardManager *)shared
{
    @synchronized([SDILeaderBoardManager class]) {
        if (!_shared) {
            [self new];
        }
    }
    return _shared;
}

+(id)alloc
{
    @synchronized([SDILeaderBoardManager class]) {
        NSAssert(_shared == nil, @"Attempled second allocation of SyncManager singleton");
        _shared = [super alloc];
    }
    
    return _shared;
}

#pragma mark - Leaderboard
- (void)reportHighScore:(NSInteger)highScore forLeaderboardIdentifier:(NSString*)identifier {
    if ([GKLocalPlayer localPlayer].isAuthenticated) {
        GKScore* score = [[GKScore alloc] initWithLeaderboardIdentifier:identifier];
        score.value = highScore;
        [GKScore reportScores:@[score] withCompletionHandler:^(NSError *error) {
            if (error) {
                NSLog(@"error: %@", error);
            }
        }];
    }
}

#pragma mark - Achievements
- (void)reportAchievementIdentifier:(NSString*)identifier percentComplete:(float)percent
{
    // Make sure the achievement has not been completed.
    NSNumber *percentComplete = (NSNumber *)[[NSUserDefaults standardUserDefaults] objectForKey:identifier];
    if (percentComplete == nil || ![percentComplete  isEqual:@100]) {
        GKAchievement *achievement = [[GKAchievement alloc] initWithIdentifier: identifier];
        if (achievement)
        {
            achievement.percentComplete = percent;
            [achievement reportAchievementWithCompletionHandler:^(NSError *error)
             {
                 if (error != nil)
                 {
                     NSLog(@"Error in reporting achievements: %@", error);
                 } else {
                     [GKNotificationBanner showBannerWithTitle:achievement.identifier message:@"Achievement earned!" duration:3 completionHandler:nil];
                     
                     NSLog(@"Reported %@ achievement success", identifier);
                     [[NSUserDefaults standardUserDefaults] setObject:@100 forKey:identifier];
                     [[NSUserDefaults standardUserDefaults] synchronize];
                 }
             }];
        }
    }
}

- (void)loadAchievements
{    [GKAchievement loadAchievementsWithCompletionHandler:^(NSArray *achievements, NSError *error) {
    if (error != nil)
    {
        // Handle the error.
    }
    if (achievements != nil)
    {
        // Process the array of achievements.
        for (GKAchievement *achievement in achievements) {
            NSNumber *percentComplete = (NSNumber *)[[NSUserDefaults standardUserDefaults] objectForKey:achievement.identifier];
            if (percentComplete == nil || ![percentComplete  isEqual:@100]) {
                [[NSUserDefaults standardUserDefaults] setObject:@100 forKey:achievement.identifier];
                [GKNotificationBanner showBannerWithTitle:achievement.identifier message:@"Achievement earned!" duration:3 completionHandler:nil];
            }
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}];
}

@end

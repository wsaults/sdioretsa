//
//  SDIConstants.m
//  sdioretsa
//
//  Created by Will Saults on 12/11/13.
//  Copyright (c) 2013 AppVentures LLC. All rights reserved.
//

#import "SDIConstants.h"

@implementation SDIConstants

NSString * const FontNamedCopperplate = @"Copperplate";

@end

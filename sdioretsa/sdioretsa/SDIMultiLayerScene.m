//
//  SDIMultiLayerScene.m
//  sdioretsa
//
//  Created by Will Saults on 11/25/13.
//  Copyright (c) 2013 AppVentures LLC. All rights reserved.
//

#import "SDIMultiLayerScene.h"
#import "SDIShip.h"
#import "SDIAsteroid.h"
#import "SDIUFOCharacter.h"
#import "SDIUtilities.h"
#import "SDILeaderBoardManager.h"

@interface SDIMultiLayerScene()

@property (nonatomic) SKNode *world;                         // Root node to which all game renderables are attached
@property (nonatomic) NSMutableArray *layers;                // Different layer nodes within the world
@property (nonatomic) NSTimeInterval lastUpdateTimeInterval; // The previous update: loop time interval

@end

@implementation SDIMultiLayerScene

@synthesize asteroids = _asteroids, ships = _ships, ufos = _ufos, shipLives = _shipLives, asteroidLives = _asteroidLives;

#pragma mark - Initialization
- (instancetype)initWithSize:(CGSize)size
{
    self = [super initWithSize:size];
    if (self) {
        _world = [[SKNode alloc] init];
        [_world setName:@"world"];
        _layers = [NSMutableArray arrayWithCapacity:kWorldLayerCount];
        for (int i = 0; i < kWorldLayerCount; i++) {
            SKNode *layer = [[SKNode alloc] init];
            layer.zPosition = i - kWorldLayerCount;
            
            [_world addChild:layer];
            [(NSMutableArray *)_layers addObject:layer];
        }
        
        [self addChild:_world];
    }
    return self;
}

#pragma mark - Characters
- (SDIShip *)addShip
{
    // Warp in ship 
    SDIShip *ship = [[SDIShip alloc] initAtPosition:CGPointMake(CGRectGetMidX(self.frame),CGRectGetMidY(self.frame))];
    if (ship) {
        SKEmitterNode *warp = [[self sharedSpawnEmitter] copy];
        warp.position = ship.position;
        [self addNode:warp atWorldLayer:SDIWorldLayerAboveCharacter];
        
        SKEmitterNode *lightning = [[self sharedSpawnEmitter2] copy];
        lightning.position = ship.position;
        [self addNode:lightning atWorldLayer:SDIWorldLayerAboveCharacter];
        
        [warp runAction:[SKAction sequence:@[[SKAction waitForDuration:.5f],[SKAction runBlock:^{
            warp.particleBirthRate = 0;
            lightning.particleBirthRate = 0;
        }],
            [SKAction waitForDuration:warp.particleLifetime + warp.particleLifetimeRange],
            [SKAction removeFromParent],
        ]]];
        
        SKAction *warpSound = [SKAction playSoundFileNamed:@"Warp.wav" waitForCompletion:NO];
        [self runAction:warpSound];
        
        [ship fadeIn:1.5];
        [ship addToScene:self];
        [self.ships addObject:ship];
    }
    return ship;
}

- (void)shipWasKilled
{
    
}

- (SDIAsteroid *)addAsteroidAtPosition:(CGPoint)position withSize:(CGSize)size withMoveDirection:(CGPoint)moveDirection
{
    SDIAsteroid *asteroid = [[SDIAsteroid alloc] initAtPosition:position withSize:size withMoveDirection:(CGPoint)moveDirection];
    if (asteroid) {
//        [asteroid fadeIn:2.0];
        [asteroid addToScene:self];
        [self.asteroids addObject:asteroid];
        NSLog(@"created asteroid %@", asteroid);
        
        if (self.asteroids.count >= 7) {
            [[SDILeaderBoardManager shared] reportAchievementIdentifier:kLuckySevenAchievementId percentComplete:100.0f];
        }
    }
    return asteroid;
}

- (SDIUFOCharacter *)addUFOAtPosition:(CGPoint)position
{
    SDIUFOCharacter *ufo = [[SDIUFOCharacter alloc] initAtPosition:position];
    if (ufo) {
        CGMutablePathRef cgpath = CGPathCreateMutable();
        // Random start values
        float xStart = boris_random(0, self.size.width);
        float xEnd = boris_random(0, self.size.width);
        // Bezier curve control point 1
        float controlPoint1X = boris_random(0, self.size.width);
        float controlPoint1Y = boris_random(0, self.size.height);
        // Bezier curve control point 2
        float controlPoint2X = boris_random(0, self.size.width);
        float controlPoint2Y = boris_random(0, controlPoint1Y);
        CGPoint start = CGPointMake(xStart, 320);
        CGPoint end = CGPointMake(xEnd, -100.0);
        CGPoint controlPoint1 = CGPointMake(controlPoint1X, controlPoint1Y);
        CGPoint controlPoint2 = CGPointMake(controlPoint2X, controlPoint2Y);
        CGPathMoveToPoint(cgpath,NULL, start.x, start.y);
        CGPathAddCurveToPoint(cgpath, NULL, controlPoint1.x, controlPoint1.y, controlPoint2.x, controlPoint2.y, end.x, end.y);
        
        SKAction *followPath = [SKAction followPath:cgpath asOffset:NO orientToPath:NO duration:3];
        
        [self addNode:ufo atWorldLayer:SDIWorldLayerBelowCharacter];
        [self.ufos addObject:ufo];
        
        [ufo runAction:[SKAction sequence:@[followPath]]];
        CGPathRelease(cgpath);
    }
    
    return ufo;
}

- (void)addNode:(SKNode *)node atWorldLayer:(SDIWorldLayer)layer
{
    SKNode *layerNode = self.layers[layer];
    [layerNode addChild:node];
}

- (void)addToScore:(uint32_t)amount
{
}

#pragma mark - Shared Assets
+ (void)loadSceneAssetsWithCompletionHandler:(SDIAssetLoadCompletionHandler)handler
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        // Load the shared assets in the background.
        [self loadSceneAssets];
        
        if (!handler) {
            return;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // Call the completion handler back on the main queue.
            handler();
        });
    });
}

+ (void)loadSceneAssets
{
    // Overridden by subclasses.
}

+ (void)releaseSceneAssets
{
    // Overridden by subclasses.
}

- (SKEmitterNode *)sharedSpawnEmitter
{
    // Overridden by subclasses.
    return nil;
}

- (SKEmitterNode *)sharedSpawnEmitter2
{
    // Overridden by subclasses.
    return nil;
}

- (SKEmitterNode *)sharedBGEmitter
{
    // Overridden by subclasses.
    return nil;
}

#pragma mark - Loop Update
- (void)update:(NSTimeInterval)currentTime
{
    // Handle time delta.
    // If we drop below 60fps, we still want everything to move the same distance.
    CFTimeInterval timeSinceLast = currentTime - self.lastUpdateTimeInterval;
    self.lastUpdateTimeInterval = currentTime;
    if (timeSinceLast > 1) { // more than a second since last update
        timeSinceLast = kMinTimeInterval;
        self.lastUpdateTimeInterval = currentTime;
    }
    
    [self updateWithTimeSinceLastUpdate:timeSinceLast];
    
    // Update movement for asteroids
    for (SDIAsteroid *asteroid in self.asteroids) {
        // If the asteroid has been destroyed then stop updating it's posistion because it is about to be removed.
        if (!asteroid || asteroid.isDying) {
            continue;
        }
        
        [self wrapSpriteIfNeeded:asteroid];
        
        CGPoint moveDirection = asteroid.moveDirection;
        if (hypotf(moveDirection.x, moveDirection.y) > 0.0f) {
            [asteroid moveInDirection:moveDirection withTimeInterval:timeSinceLast];
        }
    }
    
    // Update movement for ships
    for (SDIShip *ship in self.ships) {
        
        if (!ship || ship.isDying) {
            continue;
        }
        
        [self wrapSpriteIfNeeded:ship];
        
        CGPoint moveDirection = ship.moveDirection;
        if (hypotf(moveDirection.x, moveDirection.y) > 0.0f) {
            [ship moveInDirection:moveDirection withTimeInterval:timeSinceLast];
        }
    }
}

-(void)wrapSpriteIfNeeded:(SKSpriteNode *)node
{
    // If the node is outside of the view bounds
    if(!CGRectContainsPoint(self.view.bounds, node.position)) {
        
        // If the node is outside of the left side
        if (node.position.x < 0) {
            [node setPosition:CGPointMake(self.view.bounds.size.width, node.position.y)];
        }
        
        // If the node is outside of the top side
        if (node.position.y < 0) {
            [node setPosition:CGPointMake(node.position.x , self.view.bounds.size.height)];
        }
        
        // If the node is outside of the right side
        if (node.position.x > self.view.bounds.size.width) {
            [node setPosition:CGPointMake(0 ,node.position.y)];
        }
        
        // If the node is outside of the bottom side
        if (node.position.y > self.view.bounds.size.height) {
            [node setPosition:CGPointMake(node.position.x , 0)];
        }
    }
}

- (void)updateWithTimeSinceLastUpdate:(NSTimeInterval)timeSinceLast
{
    // Overridden by subclasses.
}

- (void)endGameWithWin:(BOOL)win
{
    // Overridden by subclasses.
}

@end

//
//  SDICharacter.m
//  sdioretsa
//
//  Created by Will Saults on 11/25/13.
//  Copyright (c) 2013 AppVentures LLC. All rights reserved.
//

#import "SDICharacter.h"
#import "SDIGameplayScene.h"
#import "SDIMultiLayerScene.h"
#import "SDIUtilities.h"

@implementation SDICharacter

- (id)initWithName:(NSString *)spriteName AtPosition:(CGPoint)position withSize:(CGSize)size
{
    if ([super init]) {
        self = [self.class spriteNodeWithImageNamed:spriteName];
        self.name = spriteName;
        [self sharedInitAtPosition:position withSize:size];
    }
    return self;
}

- (id)initWithTexture:(SKTexture *)texture atPosition:(CGPoint)position
{
    self = [super initWithTexture:texture];
    
    if (self) {
        [self sharedInitAtPosition:position];
    }
    
    return self;
}

- (void)sharedInitAtPosition:(CGPoint)position withSize:(CGSize)size
{
    self.size = size;
    [self sharedInitAtPosition:position];
}

- (void)sharedInitAtPosition:(CGPoint)position
{
    self.position = position;
    self.anchorPoint = CGPointMake(0.5,0.5);
    
    _health = 100.0f;
    _movementSpeed = kMovementSpeed;
    _animated = YES;
    _animationSpeed = 1.0f/28.0f;
    
    [self configurePhysicsBody];
}

- (void)reset
{
    // Reset some base states (used when recycling character instances).
    self.health = 100.0f;
    self.dying = NO;
    self.attacking = NO;
    self.animated = YES;
    self.requestedAnimation = SDIAnimationStateIdle;
}

/* Overridden Methods. */
- (void)animationDidComplete:(SDIAnimationState)animationState
{
}

- (void)collidedWith:(SKPhysicsBody *)other
{
    // Handle a collision with another character, projectile, etc (usually overidden).
}

- (void)performDeath
{
    self.health = 0.0f;
    self.dying = YES;
    self.requestedAnimation = SDIAnimationStateDeath;
}

- (void)configurePhysicsBody
{
    // Overridden by subclasses to create a physics body with relevant collision settings for this character.
}

#pragma mark - Loop Update
- (void)updateWithTimeSinceLastUpdate:(CFTimeInterval)interval
{
    if (self.isAnimated) {
        [self resolveRequestedAnimation];
    }
}

#pragma mark - Animation
- (void)resolveRequestedAnimation
{
    // Determine the animation we want to play.
    NSString *animationKey = nil;
    NSArray *animationFrames = nil;
    SDIAnimationState animationState = self.requestedAnimation;
    
    switch (animationState) {
            
        default:
            break;
            
        case SDIAnimationStateMove:
            animationKey = @"anim_move";
            animationFrames = [self moveAnimationFrames];
            break;
    }
    
    if (animationKey) {
        [self fireAnimationForState:animationState usingTextures:animationFrames withKey:animationKey];
    } else {
        [self animationDidComplete:animationState];
    }
}

- (void)fireAnimationForState:(SDIAnimationState)animationState usingTextures:(NSArray *)frames withKey:(NSString *)key {
    SKAction *animAction = [self actionForKey:key];
    if (animAction || [frames count] < 1) {
        return; // we already have a running animation or there aren't any frames to animate
    }
    
    self.activeAnimationKey = key;
    [self runAction:[SKAction sequence:@[
                                         [SKAction animateWithTextures:frames timePerFrame:self.animationSpeed resize:YES restore:NO],
                                         [SKAction runBlock:^{
        [self animationHasCompleted:animationState];
    }]]] withKey:key];
}

- (void)fadeIn:(CGFloat)duration
{
    // Fade in the sprite.
    SKAction *fadeAction = [SKAction fadeInWithDuration:duration];
    
    self.alpha = 0.0f;
    [self runAction:fadeAction];
}

- (void)animationHasCompleted:(SDIAnimationState)animationState {
    if (self.dying) {
        self.animated = NO;
    }
    
    [self animationDidComplete:animationState];
    
    self.activeAnimationKey = nil;
}

- (void )fireProjectile
{
    // Overridden by subclasses to return a suitable projectile.
}

- (SKSpriteNode *)projectile
{
    // Overridden by subclasses to return a suitable projectile.
    return nil;
}

- (SKEmitterNode *)projectileEmitter
{
    // Overridden by subclasses to return the particle emitter to attach to the projectile.
    return nil;
}

#pragma mark - Working with Scenes
- (void)addToScene:(SDIMultiLayerScene *)scene
{
    [scene addNode:self atWorldLayer:SDIWorldLayerCharacter];
}

- (void)removeFromParent
{
    [super removeFromParent];
}

- (SDIMultiLayerScene *)characterScene
{
    SDIMultiLayerScene *scene = (id)[self scene];
    
    if ([scene isKindOfClass:[SDIMultiLayerScene class]]) {
        return scene;
    } else {
        return nil;
    }
}

#pragma mark - Orientation and Movement
- (void)move:(SDIMoveDirection)direction withTimeInterval:(NSTimeInterval)timeInterval
{
    NSLog(@"move to");
}

- (CGFloat)faceTo:(CGPoint)position withDuration:(CGFloat)duration
{
    CGFloat ang = SDI_POLAR_ADJUST(SDIRadiansBetweenPoints(position, self.position));
    SKAction *action = [SKAction rotateToAngle:ang duration:duration];
    [self runAction:action];
    return ang;
}

- (void)evade:(CGPoint)direction withTimeInterval:(NSTimeInterval)timeInterval
{
    CGPoint curPosition = self.position;
    
    // If the enemy is near then begin evading.
    if (SDIDistanceBetweenPoints(curPosition, direction) < 60) {
        CGFloat dx = direction.x - curPosition.x;
        CGFloat dy = direction.y - curPosition.y;
        CGFloat dt = self.movementSpeed * timeInterval;
        
        CGPoint targetPosition = CGPointMake(curPosition.x - dx, curPosition.y - dy);
        
        CGFloat ang = SDI_POLAR_ADJUST(SDIRadiansBetweenPoints(targetPosition, curPosition));
        self.zRotation = ang;
        
        CGFloat distRemaining = hypotf(dx, dy);
        if (distRemaining < dt) {
            self.position = targetPosition;
        } else {
            self.position = CGPointMake(curPosition.x - sinf(ang * -1)*dt,
                                        curPosition.y + cosf(ang * -1)*dt);
        }
    }
}

- (void)moveTowards:(CGPoint)position withTimeInterval:(NSTimeInterval)timeInterval
{
    CGPoint curPosition = self.position;
    CGFloat dx = position.x - curPosition.x;
    CGFloat dy = position.y - curPosition.y;
    CGFloat dt = self.movementSpeed * timeInterval;
    
    CGFloat ang = SDI_POLAR_ADJUST(SDIRadiansBetweenPoints(position, curPosition));
    self.zRotation = ang;
    
    CGFloat distRemaining = hypotf(dx, dy);
    if (distRemaining < dt) {
        self.position = position;
    } else {
        self.position = CGPointMake(curPosition.x - sinf(ang)*dt,
                                    curPosition.y + cosf(ang)*dt);
    }
}

- (void)moveInDirection:(CGPoint)direction withTimeInterval:(NSTimeInterval)timeInterval
{
    CGPoint curPosition = self.position;
    CGFloat movementSpeed = self.movementSpeed;
    CGFloat dx = movementSpeed * direction.x;
    CGFloat dy = movementSpeed * direction.y;
    CGFloat dt = movementSpeed * timeInterval;
    
    CGPoint targetPosition = CGPointMake(curPosition.x + dx, curPosition.y + dy);
    
    CGFloat ang = SDI_POLAR_ADJUST(SDIRadiansBetweenPoints(targetPosition, curPosition));
    self.zRotation = ang;
    
    CGFloat distRemaining = hypotf(dx, dy);
    if (distRemaining < dt) {
        self.position = targetPosition;
    } else {
        self.position = CGPointMake(curPosition.x - sinf(ang)*dt,
                                    curPosition.y + cosf(ang)*dt);
    }
}

#pragma mark - Shared Assets
+ (void)loadSharedAssets
{
    // overridden by subclasses
}

- (NSArray *)moveAnimationFrames {
    return nil;
}

static SKAction *sSharedProjectileSoundAction = nil;
- (SKAction *)projectileSoundAction
{
    return sSharedProjectileSoundAction;
}

static SKEmitterNode *sSharedDeathEmitter = nil;
- (SKEmitterNode *)deathEmitter
{
    return sSharedDeathEmitter;
}

@end

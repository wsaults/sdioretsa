//
//  SDIEnemyCharacter.m
//  sdioretsa
//
//  Created by Will Saults on 12/2/13.
//  Copyright (c) 2013 AppVentures LLC. All rights reserved.
//

#import "SDIEnemyCharacter.h"
#import "SDIArtificialIntelligence.h"

@implementation SDIEnemyCharacter

#pragma mark - Loop Update
- (void)updateWithTimeSinceLastUpdate:(CFTimeInterval)interval {
    [super updateWithTimeSinceLastUpdate:interval];
    
    [self.intelligence updateWithTimeSinceLastUpdate:interval];
}

- (void)animationDidComplete:(SDIAnimationState)animationState {
    if (animationState == SDIAnimationStateAttack) {
        // tell the target that we collided with it.
        [self.intelligence.target collidedWith:self.physicsBody];
    }
}

@end
